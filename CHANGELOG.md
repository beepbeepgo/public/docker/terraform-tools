# 1.0.0 (2022-12-16)


### Bug Fixes

* include python and awscli in docker file ([87d4235](https://gitlab.com/beepbeepgo/public/docker/terraform-tools/commit/87d423575cd990419f1f1a62d4bd1f634e8edcc2))
* new release process ([b8366b3](https://gitlab.com/beepbeepgo/public/docker/terraform-tools/commit/b8366b3dfd9196dd307a6b0edf51f66d4e2bec61))
* remove user nobody ([13bdd81](https://gitlab.com/beepbeepgo/public/docker/terraform-tools/commit/13bdd81f36fd77e2a057984fd329da6262bf0be8))
* update image ([c633c2f](https://gitlab.com/beepbeepgo/public/docker/terraform-tools/commit/c633c2feb24504f8ca9859db9f88ebf9c2a28c91))
* update package git ref ([e1454c6](https://gitlab.com/beepbeepgo/public/docker/terraform-tools/commit/e1454c67f96186633ebf1f8e4d88bce73450b1ce))
* update sr ([1d0ccb1](https://gitlab.com/beepbeepgo/public/docker/terraform-tools/commit/1d0ccb19f85c16d29e57a9262da74e97ef7af26c))


### Features

* image with gitlab-terraform and tfsec installed ([c34727e](https://gitlab.com/beepbeepgo/public/docker/terraform-tools/commit/c34727eaccdb8b5a9ecebe0c90d2b77bcc424e50))

# [1.0.0-issue-update-sr-package.2](https://gitlab.com/beepbeepgo/public/docker/terraform-tools/compare/1.0.0-issue-update-sr-package.1...1.0.0-issue-update-sr-package.2) (2022-12-16)


### Bug Fixes

* bump semrel version ([5a6b2de](https://gitlab.com/beepbeepgo/public/docker/terraform-tools/commit/5a6b2deadeaba4dfc294c2fe0dfb2f412a31375f))

# 1.0.0-issue-update-sr-package.1 (2022-12-16)


### Bug Fixes

* bump major version of sr ([52974aa](https://gitlab.com/beepbeepgo/public/docker/terraform-tools/commit/52974aa6692c06390427eab5208c2c7200fa5663))
* include python and awscli in docker file ([87d4235](https://gitlab.com/beepbeepgo/public/docker/terraform-tools/commit/87d423575cd990419f1f1a62d4bd1f634e8edcc2))
* new release process ([b8366b3](https://gitlab.com/beepbeepgo/public/docker/terraform-tools/commit/b8366b3dfd9196dd307a6b0edf51f66d4e2bec61))
* remove user nobody ([13bdd81](https://gitlab.com/beepbeepgo/public/docker/terraform-tools/commit/13bdd81f36fd77e2a057984fd329da6262bf0be8))
* update image ([c633c2f](https://gitlab.com/beepbeepgo/public/docker/terraform-tools/commit/c633c2feb24504f8ca9859db9f88ebf9c2a28c91))
* update package git ref ([e1454c6](https://gitlab.com/beepbeepgo/public/docker/terraform-tools/commit/e1454c67f96186633ebf1f8e4d88bce73450b1ce))


### Features

* image with gitlab-terraform and tfsec installed ([c34727e](https://gitlab.com/beepbeepgo/public/docker/terraform-tools/commit/c34727eaccdb8b5a9ecebe0c90d2b77bcc424e50))

# 1.0.0 (2022-12-15)


### Bug Fixes

* include python and awscli in docker file ([87d4235](https://gitlab.com/beepbeepgo/public/docker/terraform-tools/commit/87d423575cd990419f1f1a62d4bd1f634e8edcc2))
* new release process ([b8366b3](https://gitlab.com/beepbeepgo/public/docker/terraform-tools/commit/b8366b3dfd9196dd307a6b0edf51f66d4e2bec61))
* remove user nobody ([13bdd81](https://gitlab.com/beepbeepgo/public/docker/terraform-tools/commit/13bdd81f36fd77e2a057984fd329da6262bf0be8))
* update image ([c633c2f](https://gitlab.com/beepbeepgo/public/docker/terraform-tools/commit/c633c2feb24504f8ca9859db9f88ebf9c2a28c91))
* update package git ref ([e1454c6](https://gitlab.com/beepbeepgo/public/docker/terraform-tools/commit/e1454c67f96186633ebf1f8e4d88bce73450b1ce))


### Features

* image with gitlab-terraform and tfsec installed ([c34727e](https://gitlab.com/beepbeepgo/public/docker/terraform-tools/commit/c34727eaccdb8b5a9ecebe0c90d2b77bcc424e50))
