FROM registry.gitlab.com/gitlab-org/terraform-images/stable:latest as builder

# hadolint ignore=DL3018,DL3019
RUN apk add python3 py3-pip

# hadolint ignore=DL3013
RUN pip3 install --no-cache-dir --upgrade pip \
    && pip3 install --no-cache-dir awscli

FROM aquasec/tfsec-ci:v1.28

COPY --from=builder . .
